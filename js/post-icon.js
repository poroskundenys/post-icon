(function ($) {
    'use strict';
    
$('#post_icon select').each(function (idx, field) {
	$(field).select2({
	    allowClear: false,
	    minimumResultsForSearch: -1
	});
    });
    
})(jQuery);
