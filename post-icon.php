<?php
/**
 * Plugin Name: Post icon
 * Description: Post icon
 * Version: 1
 * Author:      Poroskun Denys
 * Text Domain: post_icon
 * Domain Path: /languages/
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class PostIcon {

    private static $instance = null;

    public static function instance() {
        if ( null == self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function __construct() {

        if ( is_admin() ) {
            $this->admin_init();
        } else {
            add_action( 'wp_enqueue_scripts', array( $this, 'load_dashicons' ) );
            add_filter( 'the_title', array( $this, 'post_icon' ), 10, 2 );
        }
    }

    public function admin_init() {
        add_action( 'plugins_loaded', array( $this, 'load_plugin_textdomain' ), 99 );
        add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'action_links' ) );
        add_action( 'admin_menu', array( $this, 'admin_menu_add_external_links_as_submenu' ) );
        add_action( 'admin_init', array( $this, 'plugin_settings' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'register_scripts' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'register_styles' ) );
    }

    public function post_icon( $title, $id ) {
        $options = get_option( 'post_icon' );
        if ( empty( $options[ 'post' ] ) ) {
            return $title;
        }
        if ( in_array( $id, $options[ 'post' ] ) ) {
            if ( $options[ 'position' ] == 'after' ) {
                $title = $title . '<span class="dashicons ' . $options[ 'icon' ] . '"></span>';
            } else {
                $title = '<span class="dashicons ' . $options[ 'icon' ] . '"></span>' . $title;
            }
        }
        return $title;
    }

    public function get_url() {
        return plugin_dir_url( __FILE__ );
    }

    public function load_plugin_textdomain() {
        load_plugin_textdomain( 'post_icon', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
    }

    public function action_links( $links ) {
        $links[] = '<a href="admin.php?page=post_icon">' . __( 'Settings', 'post_icon' ) . '</a>';

        return $links;
    }

    function load_dashicons() {
        wp_enqueue_style( 'dashicons' );
    }

    public function register_scripts() {
        wp_enqueue_script( 'post-icon-select2', $this->get_url() . 'js/select2.min.js', array( 'jquery' ), '', true );
        wp_enqueue_script( 'amphtml', $this->get_url() . 'js/post-icon.js', array(
            'jquery',
            'post-icon-select2'
        ), '', true );
    }

    public function register_styles() {
        wp_register_style( 'post-icon-admin-select2', $this->get_url() . 'css/select2.min.css' );
        wp_enqueue_style( 'post-icon-admin-select2' );
    }

    public function plugin_settings() {

        register_setting( 'post_icon', 'post_icon' );

        add_settings_section( 'post_icon', __( 'Main Settings', 'post_icon' ), '', 'post_icon' );

        add_settings_field( 'post', __( 'Post', 'post_icon' ), array( $this, 'form_post' ), 'post_icon', 'post_icon' );
        add_settings_field( 'icon', __( 'Icon', 'post_icon' ), array( $this, 'form_icon' ), 'post_icon', 'post_icon' );
        add_settings_field( 'position', __( 'Position', 'post_icon' ), array( $this, 'form_position' ), 'post_icon', 'post_icon' );
    }

    public function form_post() {

        $val            = get_option( 'post_icon' );
        $current_values = $val[ 'post' ] ? $val[ 'post' ] : array();
        $posts          = get_posts( 'numberposts=-1' );
        ?>
        <label for="post">
            <select style="width: 30%" id="post" name="post_icon[post][]" multiple size="5">
                <?php ?>
                <?php foreach ( $posts as $post ): ?>
                    <option
                        value="<?php echo $post->ID ?>" <?php selected( in_array( $post->ID, $current_values ) ) ?>>
                            <?php echo $post->post_title ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </label>        
        <?php
    }

    public function form_icon() {
        $val = get_option( 'post_icon' );
        $val = $val ? $val[ 'icon' ] : '';
        ?>
        <input type="text" name="post_icon[icon]" value="<?php echo $val ?>"/>
        <?php
    }

    public function form_position() {

        $val           = get_option( 'post_icon' );
        $current_value = $val ? $val[ 'position' ] : null;
        $positions     = array( 'before', 'after' );
        ?>
        <label for="post">
            <select style="width: 30%" id="post" name="post_icon[position]" >
                <?php ?>
                <?php foreach ( $positions as $position ): ?>
                    <option
                        value="<?php echo $position ?>" <?php selected( $position, $current_value ) ?>>
                            <?php echo $position ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </label>        
        <?php
    }

    public function admin_menu_add_external_links_as_submenu() {
        add_options_page(
        esc_html__( 'Post icon', 'post_icon' ),
                    esc_html__( 'Post icon', 'post_icon' ),
                                'read',
                                'post_icon',
                                array( $this, 'options_page' ),
                                'dashicons-shield'
        );
        register_setting( 'post_icon', 'post_icon', $sanitize_callback );
    }

    public function options_page() {
        ?>
        <div class="wrap">
            <h1><?php esc_html_e( 'Post icon settings', 'post_icon' ); ?></h1>
            <form id="post_icon" method="post" action="options.php">                
                <?php
                settings_fields( 'post_icon' );
                do_settings_sections( 'post_icon' );
                submit_button();
                ?>
            </form>            
        </div>
        <?php
    }

}

function PostIcon() {
    return PostIcon::instance();
}

PostIcon();
